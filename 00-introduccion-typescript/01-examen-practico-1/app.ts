// Uso de Let y Const
let nombre: string = "Ricardo Tapia";
let edad: number = 23;
const PERSONAJE: { nombre: string, edad: number } = {
    nombre: nombre,
    edad: edad
};

console.log(nombre, edad, PERSONAJE);

// Cree una interfaz que sirva para validar el siguiente objeto
interface Heroe {
    nombre: string;
    artesMarciales: string[]
}

var batman: Heroe = {
    nombre: "Bruno Díaz",
    artesMarciales: ["Karate", "Aikido", "Wing Chun", "Jiu-Jitsu"]
};
console.log(batman);


// Convertir esta funcion a una funcion de flecha
// function resultadoDoble(a, b) {
//     return (a + b) * 2
// }

let resultadoDobleFlecha = (a: number, b: number) => (a + b) * 2;
// console.log(`resultadoDoble : ${resultadoDoble(5, 6)}`);
console.log(`resultadoDobleFlecha : ${resultadoDobleFlecha(5, 6)}`);


// Función con parametros obligatorios, opcionales y por defecto
// donde NOMBRE = obligatorio
//       PODER  = opcional
//       ARMA   = por defecto = "arco"
// function getAvenger(nombre, poder, arma) {
//     var mensaje;
//     if (poder) {
//         mensaje = nombre + " tiene el poder de: " + poder + " y un arma: " + arma;
//     } else {
//         mensaje = nombre + " tiene un " + poder
//     }
//     return mensaje;
// };

function getAvengerFlecha(nombre: string, poder?: string, arma: string = "arco") {
    if (poder) {
        return `${nombre} tiene el poder de: ${poder} y un arma: ${arma}`
    } else {
        return `${nombre} tiene un ${poder}`;
    }
}

// console.log(`getAvenger : ${getAvenger("IronMardito", "Ciencia", "Traje")}`);
console.log(`getAvengerFlecha : ${getAvengerFlecha("IronMardito", "Ciencia", "Traje")}`);

// Cree una clase que permita manejar la siguiente estructura
// La clase se debe de llamar rectangulo,
// debe de tener dos propiedades:
//   * base
//   * altura
// También un método que calcule el área  =  base * altura,
// ese método debe de retornar un numero.
class Rectangulo {
    base: number;
    altura: number;

    constructor(base: number, altura: number) {
        this.base = base;
        this.altura = altura;
    }

    calcularArea = (): number => {
        return ((this.base + this.altura) * 2);
    };
}


let rec1 = new Rectangulo(3, 4);
console.log(`rec1.calcularArea2 : ${rec1.calcularArea()}`);