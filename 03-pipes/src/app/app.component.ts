import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pipes';

  nombre = 'Capitan America';
  nombreCompleto = 'sebAStiaN MorA EsPINOza';
  arreglo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  PI: number = Math.PI;
  porcentaje = 0.234;
  salario = 1234.5;
  fecha: Date = new Date();

  idioma = 'es';
  videoUrl = 'https://www.youtube.com/embed/PEkp7gqG9uw';
  activar = true;

  heroe = {
    nombre: 'logan',
    clave: 'Wolverine',
    edad: 500,
    direccion: {
      calle: 'Primera',
      casa: 20
    }
  };

  valorPromesa = new Promise((resolve) => {
    setTimeout(() => {
      resolve('llego la data');
    }, 4500);
  });

  setIdioma(idioma: string) {
    this.idioma = idioma;
  }
}
