import {Component, OnInit} from '@angular/core';
import {Heroe, HeroesService} from '../../services/heroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {

  private heroes: Heroe[];

  constructor(private _heroeService: HeroesService,
              private router: Router) {
  }

  ngOnInit() {
    this.heroes = this._heroeService.getHeroes();
  }

  verHeroe(idx: number) {
    console.log(idx);
    this.router.navigate(['/heroe', idx]);
  }

}
