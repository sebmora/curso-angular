import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html'
})
export class BodyComponent implements OnInit {

  frase: any = {
    mensaje: 'un gran poder conlleva una gran responsabilidad',
    autor: 'Ben Parker'
  };
  mostrar = false;
  personajes: string[] = ['Spiderman', 'Venom', 'Dr. Octopus'];

  ngOnInit(): void {
  }

}
